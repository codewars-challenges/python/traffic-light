def update_light(current):
    return {'green': 'yellow', 'yellow': 'red', 'red': 'green'}[current]


# tests
print(update_light('green'))
print(update_light('yellow'))
print(update_light('red'))
